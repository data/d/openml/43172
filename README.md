# OpenML dataset: sarcos

https://www.openml.org/d/43172

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset Description**

The data relates to an inverse dynamics problem for a seven degrees-of-freedom
SARCOS anthropomorphic robot arm. The task is to map from a 21-dimensional
input space (7 joint positions, 7 joint velocities, 7 joint accelerations) to
the corresponding 7 joint torques. Usually, the first of those (V22) is used as
the target variable and is therefore set as the default target variable, while
the other 6 joint torques are excluded from the model.

**Related Studies**

  * LWPR: An O(n) Algorithm for Incremental Real Time Learning in High
  Dimensional Space, S. Vijayakumar and S. Schaal, Proc ICML 2000, 1079-1086

  * (2000). Statistical Learning for Humanoid Robots, S. Vijayakumar, A.
  D'Souza, T. Shibata, J. Conradt, S. Schaal, Autonomous Robot, 12(1) 55-69

  * (2002) Incremental Online Learning in High Dimensions S. Vijayakumar, A.
  D'Souza, S. Schaal, Neural Computation 17(12) 2602-2634 (2005)

  * (2019) Cascaded Gaussian Processes for Data-efficient Robot Dynamics Learning,
  Sahand Rezaei-Shoshtari, David Meger, Inna Sharf, https://arxiv.org/pdf/1910.02291.pdf

  * (2019) TabNet: Attentive Interpretable Tabular Learning,
  Sercan Oe. Arik, Tomas Pfister, https://arxiv.org/pdf/1908.07442.pdf




**Citation**

LWPR: An O(n) Algorithm for Incremental Real Time Learning in High Dimensional Space, S. Vijayakumar and S. Schaal, Proc ICML 2000, 1079-1086 (2000).

The data was obtained from: http://www.gaussianprocess.org/gpml/data/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43172) of an [OpenML dataset](https://www.openml.org/d/43172). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43172/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43172/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43172/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

